---
layout: markdown_page
title: "GitHub"
---
<!-- This is the template for sections to include and the order to include
them. If a section has no content yet then leave it out. Leave this note in
tact so that others can see where new sections should be added.

### Summary
### Strengths
### Challenges
### Who buys and why
### Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
### Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
### FAQs
 - about the product  <-- comment. delete this line
### Integrations
### Pricing
   - summary, links to tool website  <-- comment. delete this line
### Comparison
   - link to comparison page  <-- comment. delete this line
### Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

### Summary

### Comments/Anecdotes
- Feedback from customers is that GitHub Enterprise (GHE) has trouble scalling. It seems like anyone nearing 2k users starts to run into issues. [GitLab is enterprise class](/enterprise-class/) and scales to > 32K users.

### Resources
* [GitHub Website](https://www.github.com/)
