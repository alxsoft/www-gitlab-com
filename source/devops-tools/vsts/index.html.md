---
layout: markdown_page
title: "Visual Studio Team Services (VSTS)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Visual Studio Team Services (VSTS) Summary
Visual Studio Team Services (VSTS) is a hosted cloud offering, and Team Foundation Server ([TFS](../tfs/)), is an on-premises platform. Essentially, VSTS is a cloud, hosted version of TFS. Both offer functionality that cover multiple stages of the DevOps lifecycle including planning tools, source code managment (SCM), and CI/CD. 

As part of their SCM functionality, both platforms offer two methods of version control.

1. [Git](https://www.visualstudio.com/team-services/git/) (distributed) - each developer has a copy on their dev machine of the source repository including all branch and history information.

2. Team Foundation Version Control ([TFVC](https://www.visualstudio.com/team-services/tfvc/)), a centralized, client-server system - developers have only one version of each file on their dev machines. Historical data is maintained only on the server.

Microsoft recommends customers use Git for version control unless there is a specific need for centralized version control features. [https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc](https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc)

### Website
[Visual Studio Team Services](https://www.visualstudio.com/team-services/)

### Comments


### Pricing

[VSTS Pricing](https://visualstudio.microsoft.com/team-services/pricing/)

Visual Studio ‘Professional Version’ is the most comparable to GitLab since Visual Studio ‘Enterprise Version’ includes extras outside the scope of DevOps (such as MS Office, etc).

Visual Studio Professional can be purchased under a ‘standard’ or ‘cloud’ model.

- Standard = $1,200 year one (retail pricing), then $800 annual renewals (retail pricing) 
- Cloud - $540 per year 

Under their ‘modern purchasing model’, the monthly cost for Visual Studio Professional (which includes TFS and CAL license) is $45 / mo ($540 / yr).  However, extensions to TFS such as [Test Manager](https://marketplace.visualstudio.com/items?itemName=ms.vss-testmanager-web) ($52/mo), [Package Management](https://marketplace.visualstudio.com/items?itemName=ms.feed) ($15/mo), and [Private Pipelines](https://marketplace.visualstudio.com/items?itemName=ms.build-release-private-pipelines) ($15/mo) require an additional purchase.