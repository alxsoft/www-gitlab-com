---
layout: markdown_page
title: "Building applications that meet HIPAA standards"
---
## See how using GitLab can help you with HIPAA compliance

The Health Insurance Portability and Accountability Act (HIPAA) sets the standard for sensitive patient data protection. 
Companies that deal with protected health information (PHI) must have physical, network, and process security measures in place and follow them to ensure HIPAA Compliance.

This page will include the rules and specific controls where GitLab capabilities and features can help.

