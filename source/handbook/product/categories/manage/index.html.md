---
layout: markdown_page
title: "Manage Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Manage
{: #welcome}

The responsibilities of this team are described by the [Manage product
category](/handbook/product/categories/#manage). Among other things, this means 
working on GitLab's functionality around user, group and project administration, 
authentication, access control, and subscriptions.

* I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the PM for the [Manage product
category](/handbook/product/categories/#dev). GitLabbers can also use #g_manage.

### How we work

* In accordance with our [GitLab values](https://about.gitlab.com/handbook/values/)
* Transparently: nearly everything is public, we should record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

#### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). 
Release scope for an upcoming release should be finalized by the `1st`.

To scope the release, Product is responsible for maintaining a prioritized list of issues. We use
[this prioritization board](https://gitlab.com/groups/gitlab-org/-/boards/759099?=&label_name[]=Manage&label_name[]=backend) 
as the SSOT. Issues are tagged with the "prioritized" label and stack-ranked in the respective column. We use the following timeline:

* On or around the `24th`: Product meets with engineering managers for a preliminary issue review. Issues are tagged with a milestone.
* On or around the `28th`: Product takes the reviewed scope to the larger team for discussion. This becomes the release scope for the next release.
* On or around the `6th`: Product meets with engineering managers for a pre-kickoff check-in.

#### During the release

Developers will start the release with assigned issues. If a developer has completed work on an issue, 
they may open the [prioritization board](https://gitlab.com/groups/gitlab-org/-/boards/759099?=&label_name[]=Manage&label_name[]=backend) 
and begin working on the next prioritized issue. Issues should be worked on in priority order 
(the "prioritized" column will always be sorted by descending priority; the issue at the top is most important).

### Links and Resources
{: #links}
* Our Slack channel
  * #g_manage
* Calendar
  * GitLabbers can add [this calendar](https://calendar.google.com/calendar/b/1?cid=Z2l0bGFiLmNvbV9rOWYyN2lqamExaGoxNzZvbmNuMWU4cXF2a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) in Google Calendar.
* Meeting agendas
  * Agendas and notes from team meetings can be found [here](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit). For transparency across the team, we should use one agenda document.
