---
layout: markdown_page
title: "2018 Q4 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: TBD IACV Goal

* VPE
  * Support:
    * EMEA:
    * APAC:
	* AMER East:
	* AMER West:

### CEO: TBD Product Goal

* VPE:
  * Frontend:
  * Dev Backend:
  * Ops Backend:
  * Infrastructure:
  * Quality:
  * UX:
  * Security:

### CEO: TBD Team Goal

* VPE
  * Frontend:
  * Dev Backend:
  * Infrastructure:
  * Ops Backend:
  * Quality:
  * Security:
  * Support:
  * UX:
